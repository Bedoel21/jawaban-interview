# 1. Monitoring resource dari komputer
- RAM       
    >Untuk memonitoring RAM, kita dapat menggunakan command :
    
        free
    >seperti yang dapat di lihat pada gambar

    >![dokumentasi](dokumentasi/monitor_RAM.gif)

    > pada gambar di atas dapat dilihat ada command :

        free --help
    > opsi **"--help"** bertujuan untuk melihat fungsi apa saja yang terdapat pada command **"free"**

        free -h
    > Opsi **"-h"** pada command **"free"** menghasilkan output yang diatur dalam satuan yang lebih mudah dipahami manusia, seperti KB, MB, dan GB. 


- CPU
    >Untuk memonitoring CPU, kita dapat menggunakan command :
        
        ps atau top 
    >seperti yang dapat di lihat pada gambar

    >![dokumentasi](dokumentasi/monitor_CPU.gif)
    
    > pada gambar di atas dapat dilihat pengimplementasian command **"ps"** :

        ps --help
    > opsi **"--help"** bertujuan untuk melihat fungsi apa saja yang terdapat pada command **"ps"**

        ps --help all
    > opsi **"--help all"** bertujuan untuk melihat semua list fungsi apa saja yang terdapat pada command **"ps"**

        ps -aux
    > Opsi "-a", "-u", dan "-x" pada perintah "ps" memiliki arti sebagai berikut:
    
        "-a": Menampilkan proses dari semua pengguna pada sistem, tidak hanya proses yang dimulai oleh pengguna yang sedang masuk ke sistem.
        "-u": Menampilkan informasi pengguna yang menjalankan proses, termasuk nama pengguna, ID pengguna, dan jumlah waktu CPU yang digunakan.
        "-x": Menampilkan semua proses, termasuk yang tidak memiliki terminal yang terkait (disebut juga proses "demon")
    
    >untuk penggunaan command **"top"** kurang lebih sama dengan command **"ps"**.
 
    
- Hardisk
     >Untuk memonitoring Hardisk, kita dapat menggunakan command :
    
        df
    >seperti yang dapat di lihat pada gambar

    >![dokumentasi](dokumentasi/monitor_Storage.gif)

    > pada gambar di atas dapat dilihat ada command :

        df --help
    > opsi **"--help"** bertujuan untuk melihat fungsi apa saja yang terdapat pada command **"df"**

        df -h
    > Opsi **"-h"** pada command **"df"** menghasilkan output yang diatur dalam satuan yang lebih mudah dipahami manusia, seperti KB, MB, dan GB. 

# 2. Manajemen program

- Mampu memonitor program yang sedang berjalan
    >untuk memonitor program berjalan kita bisa menggunakan command yang sama pada saat memonitor CPU, yakni
        
        ps atau top

- Mampu menghentikan program yang sedang berjalan
    > untuk menghentikan program yang sedang berjalan kita bisa menggunakan command :

        Kill
    
    > berikut merupakan command **"kill"**  jika menggunakan **"top"** :

    >![dokumentasi](dokumentasi/menghentikan_prog.gif)

    > pada gambar terlihat untuk menggunakan command **"kill"** pada **"top"** kita hanya tinggal mengklik tobol **"K"**  pada _keyboard_:

        klik tombol "K" lalu masukkan nomor PID

    > berikut merupakan command **"kill"**  jika menggunakan **"ps"** :

    >![dokumentasi](dokumentasi/menghentikan_prog_ps.gif)

    > pada gambar terlihat untuk menggunakan command **"kill"** pada **"ps"** kita hanya tinggal mengetik :

        kill <PID>
    >_Note :_
        
        1. Kill hanya bisa di lakukan oleh pemilik server
        2. dalam command **"top"**, kita bisa mengketik angka :
            - Klik 9 untuk menghentikan program secara paksa
            - Klik 15 untuk menghentikan program secara default

- Otomasi perintah dengaan shell script
    > berikut adalah proses pembuatan shell script untuk otomatis perintah :

    >![dokumentasi](dokumentasi/otomatis_shell.gif)
    > pada kasus kali ini kita membuat shell script **"otomatis.sh"**, dimana programnya di buat untuk bisa membuat folder dan file sekaligus.
    > pembuatannya menggunakan command :

        touch <nama file> maka touch otomatis.sh
    > setelah itu, membuat isinya menggunakan vim, dapat di akses dengan command :

        vim <nama file> maka vim otomatis.sh 
    > selanjutnya file di isi sesuai dengan gambar diatas.
    > lalu untuk mengeksekusi program **"otomatis.sh"**, kita meggunakan command :
    
        ./<nama file> maka ./otomatis.sh
    > lalu masukkan parameter nya,

        ./otomatis.sh kamar lemari
        ./<nama file> <parameter 1> <parameter 2> 
        ./<eksekusi> <$1> <$2>
    > seperti pada gambar, karena $1 adalah nama untuk folder, maka **"kamar"** menjadi nama folder yang baru dibuat.

    > dan sama, karena $2 adalah nama untuk file, maka **"lemari"** menajadi nama file yang baru dibuat.

    >_Note :_
        setiap parameter di pisahkan oleh space
        jadi <nama file> <parameter 1> <parameter 2> <parameter 3> <dst...>
    

- Penjadwalan eksekusi program dengan cron
    >untuk penjadawalan program dengan cron kita bisa menggunakan command :

        crontab -e
    > untuk mengedit penjadawalannya, seperti pada gamabar :

    >![dokumentasi](dokumentasi/awal_crontab.gif) 
    > seperti yang terlihat saya menjadwalkan akan ada program yang tebuat setelah 5 menit crontab terinstal yakni **"cosina.log"**, dengan command :

        5 * * * *  touch /home/praktikumc/1217050001-bedoel/cosina.log
    
    > lalu setelah 5 menit maka akan muncul, seperti yang terlihat pada gambar :

    
    >![dokumentasi](dokumentasi/akhir_crontab.gif)
    > terlihat sudah muncul file baru yakni **"cosina.log"**

    >_Note :_ 
        
            * * * * *
            - - - - -
            | | | | |
            | | | | ----- Hari dalam seminggu (0 - 7) (Minggu adalah 0 atau 7)
            | | | ------- Bulan (1 - 12)
            | | --------- Hari dalam bulan (1 - 31)
            | ----------- Jam (0 - 23)
            ------------- Menit (0 - 59)



# 3. Manajemen network

- Mengakses sistem operasi pada jaringan menggunakan SSH 

    > Untuk mengakses sistem operasi dengan SSH adalah menggunakan command berikut:

    > jika menggunakan ip

        ssh <account>@<public-ip>

    > jika di tambahkan dengan port
    
        ssh <account>@<public-ip> -p <port>

    > berikut merupakan contoh mengakses sistem operasi dengan SSH

    ![dokumentasi](dokumentasi/login_ssh.gif)

- Mampu memonitor program yang menggunakan network
    > untuk menampilkan informasi tentang koneksi jaringan, antarmuka jaringan, dan statistik jaringan. kita dapat menggunakan command :

        netstat
    > seperti pada gambar di bawah :

    ![dokumentasi](dokumentasi/network.gif)

    > pada gambar di atas dapat dilihat pengimplementasian command **"netstat"** :

        netstat --help
    > opsi **"--help"** bertujuan untuk melihat fungsi apa saja yang terdapat pada command **"netstat"**

        netstat -tulpn
    > Opsi **"-t", "-u", "l", "p"dan "-n"** pada perintah "netstat" memiliki arti sebagai berikut:
    
        "-t" menunjukkan bahwa netstat akan menampilkan informasi tentang koneksi TCP (Transmission Control Protocol).
        "-u" menunjukkan bahwa netstat akan menampilkan informasi tentang koneksi UDP (User Datagram Protocol).
        "-l" menunjukkan bahwa netstat akan menampilkan daftar port yang sedang mendengarkan (listening).
        "-p" menunjukkan bahwa netstat akan menampilkan nama proses yang terkait dengan koneksi tersebut.
        "-n" menunjukkan bahwa netstat akan menampilkan informasi numerik, seperti nomor port, alamat IP, dll.
    >

        netstat -tulpn | grep listen

    >command **"netstat -tulpn | grep LISTEN"** memiliki arti sebagai berikut:

        "netstat -tulpn": perintah ini akan menampilkan daftar koneksi jaringan dan port yang sedang digunakan oleh sistem, dengan rincian yang telah dijelaskan sebelumnya.

        "|": simbol ini disebut sebagai "pipe" dan berfungsi untuk menghubungkan output dari perintah pertama dengan perintah kedua.

        "grep LISTEN": perintah ini akan mencari baris yang mengandung kata "LISTEN" pada output yang dihasilkan dari perintah "netstat -tulpn".

- Mampu mengoperasikan HTTP client

    >berikut merupakan command yand di gunakan untuk Mengambil atau mengunduh file atau halaman web dari internet:

       curl https://www.example.com
    > berikut contoh penggunaan command tertera pada gambar di bawah

    ![dokumentasi](dokumentasi/https_client.gif)

# 4. Manajemen file dan folder

- Navigasi
    >untuk navigasi kita bisa menggunakan command :

    >untuk mengganti direktori, ada:
        
        cd
    >untuk menampilkan semua file dan folder yang ada dalam direktori, ada:
        
        ls
    >untuk menampilkan direktori yang sedang di tempati, ada:

        pwd
    > berikut merupakan gambar contoh pengimplementasian command - command di atas:
    ![dokumentasi](dokumentasi/Navigasi.gif)

- CRUD file dan folder
    >untuk  CRUD kita bisa menggunakan command:
    >untuk membuat direktori, ada:

        mkdir 
    >untuk membuat file, ada:
        
        touch
    >menghapus file atau direktori, ada:

        rm
    >cut/move file atau direktori, ada:

        mv
    >copy file atau direktori Manajemen, ada: 

        cp    
    >berikut merupakan gambar contoh pengimplementasian command - command di atas:
    ![dokumentasi](dokumentasi/CRUD_file.gif)

- manajemen ownership dan akses file dan folder
    >Mengubah ownership dari sebuah file atau folder dilakukan dengan command: 
        
         chown <NAMA_OWNER>:<NAMA_GROUP> <NAMA_FILE_ATAU_FOLDER>

    >berikut merupakan gambar pengimplementasian dari command **"chown"**:
    ![dokumnentasi](dokumentasi/chown.gif)

    >_Note:_ seperti yang terlihat eksekusi **"chown"**, tidak berhasil karean kita bukan pemilik server

    >selanjutnya, mengubah akses suatu file atau folder dilakukan dengan command:

        chmod 000 ./nama-file
   
    >**urutan angka pada parameter chmod**

    | user   | urutan angka |
    | ------ | ------       |
    | Owner  |      1       | 
    | Grup   |      2       |
    | World  |      3       |

    >**komponen angka pada parameter chmod**

    | mode    | deskripsi             |urutan angka |
    | ------  | ------                |---------    |
    | read    |  membaca isi file     |   4         | 
    | write   |  menulis isi file     |   2         |
    | execute |  menjalankan isi file |   1         |

    > maka jika kita menginputkan parameter :
        chmod 777./ kamar

        maka Owner = bisa read + write + execute = 4 + 2 + 1 = 7
        maka Grup = bisa read + write + execute = 4 + 2 + 1 = 7
        maka World = bisa read + write + execute = 4 + 2 + 1 = 7
    
    >seperti pada gamabr di bawah:
    ![dokumentasi](dokumentasi/chmod.gif)


- Pencarian file dan folder
    > untuk mecari sebuah file atau folder kita bisa menggunakan command:

        find <nama file>
    > tapi command ini hanya berpengaruh jika kita mencari file di dalam directory kita **sekarang**, jika file berda di luar direktori **sekarang**, maka file tidak akan terdeteksi

    >maka dari itu ada command:

        sudo find / name "*nama file*"
    >seperti yang terlihat pada gambar:
    ![dokumentasi](dokumentasi/find.gif)

    >terlihat bahwa untuk menggunakan sudo harus memasukkan password **Sudo**, yang seharusnya di miliki oleh pemilik owner.

- Kompresi data
    >untuk mengkompresi sebuah file atau folder kita bisa menggunakan command:

        tar atau gzip
    >pertama jika kita ingin menggunakan command **"tar"**, maka :

        tar -cf <nama file.tar> <file yang mau di kompres>
    >berikut contoh penggunaan tar:
    ![dokumentasi](dokumentasi/bikin_tar.gif)

    >selanjutnya untuk membuka file **"tar"**, gunakan command:

        tar -xf <nama file.tar>
    >maka file **"tar"** akan kembali ter exstract, seperti berikut :
    ![dokumentasi](dokumentasi/buka_tar.gif)

    >selanjutnya jika menggunakan command **"gzip"**, maka:

        gzip <nama file>
    >maka file akan akan langsung berubah menajdi **.zp**, tanpa membuat salinan file sebelum terkompres.seperti pada gambar :
    ![dokumentasi](dokumentasi/buat_gzip.gif)

    >selanjutnya, untuk membuka file **"gzip"**, kita gunakan command:

        gunzip <nama file.zp>
    >seperti pada gambar:
    ![dokumentasi](dokumentasi/buka_gzip.gif)

    >Note :
     
        tar membuat salinan, dan salinan itulah yagn menjadi file Kompresi
        gzip tidak membuat salian, jadi file asli lah yang di kompres, maka dari itu ukuran gzip bisa lebih kecil dari ukuran aslinya.

# 5. Mampu membuat program berbasis CLI menggunakan shell script
- shelll script
    >berikut lampiran shell script sederhana yang saya buat:
    ![dokumentasi](dokumentasi/buat_shell_script.gif)

    >penjelasan serta pembuatannya sudah ada di youtube (di nomor 6)

# 6. Mendemonstrasikan program CLI yang dibuat kepada publik dalam bentuk Youtube
- demostrasi program CLI dengan shell script
    >berikut link youtube demonstrasi milik saya:

    https://youtu.be/nyL2r_mbP_M

# 7. Maze challenge
- tantangan membuat maze game
    > berikut lampiran isi maze game saya:
    ![dokumentasi](dokumentasi/maze_game.gif)

    >dan berikut link maze game miik saya:
    https://maze.informatika.digital/maze/zezecave/

# 8. Inisiatif yang dibuat dalam mengerjakan project, misal penggunaan tools, ilustrasi kreatif, penggunaan operating system, penggunaan cloud, dsb.
- Tools yang digunakan dalam membantu dokumentasi :
    > ScreentoGif
